package be.multtimedi.smarthome.repository;


import be.multtimedi.smarthome.domain.Kamer;
import be.multtimedi.smarthome.domain.Light;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LightRepository extends CrudRepository<Light, Long> {
    List<Light> findLightByKamer(Kamer Kamer);
    Light findLightById(long id);

}
