package be.multtimedi.smarthome.repository;

import be.multtimedi.smarthome.domain.Kamer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KamersRepository extends CrudRepository<Kamer, Long>
{
   Kamer findKamerById(long id);
}
