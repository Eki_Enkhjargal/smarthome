package be.multtimedi.smarthome.exception;

public class LightsAppException extends Exception {

    public LightsAppException(String message) {
        super(message);
    }
}
