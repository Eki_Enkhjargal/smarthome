package be.multtimedi.smarthome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.logging.Logger;

@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled = true)
@EnableTransactionManagement
@EnableAspectJAutoProxy
public class SmarthomeApplication  extends WebSecurityConfigurerAdapter {

    @Autowired
    private Logger log;
    public static void main(String[] args) {
        SpringApplication.run(SmarthomeApplication.class, args);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http    .csrf().disable()
                .formLogin().disable()
                .httpBasic()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
