package be.multtimedi.smarthome.aspect;

import be.multtimedi.smarthome.domain.Light;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Component
@Aspect
public class TimeAspect {
   private int startHour;
   private int endHour;
   private Light light;

   @Value("${timeAspect.startHour}")
   public void setStartHour(int startHour) {
      this.startHour = startHour;
   }

   @Value("${timeAspect.endHour}")
   public void setEndHour(int endHour) {
      this.endHour = endHour;
   }
      
   @Before("execution(* *.lightToevoegen(..))")
   public void checkTime() {
      LocalTime now = LocalTime.now();
      int hour = now.getHour();
      if(hour < startHour || hour >= endHour) {
//         light.setIsAan("uit");
         System.out.println("lights Off");
      }
      else {
      //   light.setIsAan("aan");
         System.out.println("Lights aan");
      }
   }
}
