package be.multtimedi.smarthome.service;


import be.multtimedi.smarthome.controllers.resources.LightResource;
import be.multtimedi.smarthome.domain.Kamer;
import be.multtimedi.smarthome.domain.Light;
import be.multtimedi.smarthome.exception.LightsAppException;
import be.multtimedi.smarthome.repository.KamersRepository;
import be.multtimedi.smarthome.repository.LightRepository;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

@Service
public class LightsService {
  //  private static final Logger LOGGER = LogManager.getLogger(LightsService.class);
    @Autowired
    private Logger logger;
    @Autowired
    private KamersRepository kamersRepository;

    @Autowired
    private LightRepository lightRepository;


    public List<Light> getAllLights(){
        List<Light> lights= (List<Light>) lightRepository.findAll();
        logger.info("bug licht  **** "+ lights.size());
        return lights;
    }
    @Transactional(readOnly=true)
    public List<Kamer> getAllKamers(){
        List<Kamer> kamerList = (List<Kamer>) kamersRepository.findAll();
        return kamerList;
    }

    public Kamer getKamerbyId(Long id){
        Kamer kamer = kamersRepository.findKamerById(id);
        logger.info(kamer.getNaam()+ "gevonden");

        return  kamer;
    }

    public List<Light> getLightsvanKamer(long kamerId){
        Kamer kamer = kamersRepository.findKamerById(kamerId);
        List<Light> lights = lightRepository.findLightByKamer(kamer);
        return  lights;
    }

    public Light lightToevoegen(LightResource lightResource) throws LightsAppException {


        if( StringUtils.isBlank(lightResource.getNaam())){
            throw new LightsAppException("light naam is niet ingevuld");

        }
        if( StringUtils.isBlank(lightResource.getKamerId().toString())){
            throw new LightsAppException("light kamer is niet ingevuld");

        }
        if( StringUtils.isBlank(lightResource.getIsAan())){
            throw new LightsAppException("light isAan is niet ingevuld");

        }
        if(lightResource.getKamerId()<0 || lightResource.getKamerId()>4){
            throw new LightsAppException("kamerId is niet correct. (1-4)");
        }
        Kamer kamer=  kamersRepository.findKamerById(lightResource.getKamerId());

        Light newlight= new Light();
        newlight.setNaam(lightResource.getNaam());
        newlight.setAan(lightResource.getIsAan());
        newlight.setKamer(kamer);
        lightRepository.save(newlight);
        return newlight;

    }
    public Light lightUpdate(long id, LightResource lightResource) throws LightsAppException {
        Light light= lightRepository.findLightById(id);
        Kamer kamer=  kamersRepository.findKamerById(lightResource.getKamerId());
        light.setKamer(kamer);
        light.setAan(lightResource.getIsAan());
        light.setNaam(lightResource.getNaam());
        lightRepository.save(light);
        return light;
    }
    public Light lightDelete(Long id) throws LightsAppException {
        Light light= lightRepository.findLightById(id) ;
        logger.info(light.getId()+" "+light.getNaam() + " is deleted ");

        lightRepository.delete(light);
        return light;
    }

    public Light lightById(Long id) throws LightsAppException {
        Light light= lightRepository.findLightById(id) ;
        return light;
    }

}
