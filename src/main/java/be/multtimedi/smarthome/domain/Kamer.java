package be.multtimedi.smarthome.domain;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name= "Kamers")
public class Kamer implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name= "kamerCode")
    private String code;

    @Column(name= "naam")
    private String naam;

    @OneToMany(mappedBy = "kamer")
    //@JsonBackReference
    private Set<Light> lights = new HashSet<Light>();

    public Kamer() {
    }

    public Kamer(long l, String naam, String code) {
        this.id = l;
        this.naam = naam;
        this.code=code;
    }

    public Kamer(String naam, String code) {
        this.naam = naam;
        this.code=code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Set<Light> getLights() {
        return lights;
    }

    public void setLights(Set<Light> lights) {
        this.lights = lights;
    }



   @Override
    public String toString() {
        return "Kamer{" + "Id"+ id+ '\''+
        ", code='" + code + '\'' +
                ", naam='" + naam + '\'' +
                '}';
    }

}

