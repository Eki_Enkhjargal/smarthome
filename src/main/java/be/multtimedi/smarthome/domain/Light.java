package be.multtimedi.smarthome.domain;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Lights")
public class Light implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "naam")
    private String naam;

    @ManyToOne
    @JoinColumn(name="kamerId")
   // @JsonManagedReference
    private Kamer kamer;

    @Column(name = "isAan")
    private String isAan;

    public Light(){

    }

    public Light(String naam, long id, String isAan) {
        this.naam = naam;
        this.id=id;
        this.isAan=isAan;
    }

    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public String getIsAan() {
        return isAan;
    }

    public void setIsAan(String isAan) {
        this.isAan = isAan;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Kamer getKamer() {
        return kamer;
    }

    public void setKamer(Kamer kamer) {
        this.kamer = kamer;
    }

    public String isAan() {
        return isAan;
    }

    public void setAan(String aan) {
        isAan = aan;
    }

   @Override
    public String toString() {
        return "Light{" +id +" "+  naam + ' ' + " " +kamer+ '}';
    }

}
