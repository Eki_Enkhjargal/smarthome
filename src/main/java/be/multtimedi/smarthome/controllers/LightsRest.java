package be.multtimedi.smarthome.controllers;


import be.multtimedi.smarthome.controllers.resources.LightResource;
import be.multtimedi.smarthome.domain.Kamer;
import be.multtimedi.smarthome.domain.Light;
import be.multtimedi.smarthome.exception.LightsAppException;
import be.multtimedi.smarthome.service.LightsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
@RestController
public class LightsRest {

  //  private static final Logger LOGGER = LogManager.getLogger(LightsRest.class);

   @Autowired
    private Logger logger;

    @Autowired
    private LightsService lightsService;

    @Autowired private JmsTemplate jmsTemplate;

    @GetMapping("/lights")
    private List<Light> findallLights(){
        return lightsService.getAllLights();
    }

    @GetMapping("/lights/{Id}")
    private Light findLightById(@PathVariable("Id") long Id) throws LightsAppException {
        return lightsService.lightById(Id);
    }



    @GetMapping(value="/lights/kamerId/{kamerId}")
   private List<Light> findLightsByKamersId(@PathVariable("kamerId") long kamerId) {
       logger.info("kamerId "+kamerId);
       Kamer kamer = lightsService.getKamerbyId(kamerId);
       logger.info("lights van "+ kamer.getNaam());
       for (Light l: lightsService.getLightsvanKamer(kamerId)
       ) {
          logger.info(l.getNaam());
           jmsTemplate.convertAndSend("springbootQueue", l);
       }

       return lightsService.getLightsvanKamer(kamerId);
   }


    @GetMapping(value="/kamers")
    private List<Kamer> findallKamers(){
        return lightsService.getAllKamers();
    }

    @GetMapping(value="/kamers/{Id}")
   private Kamer findKamerbyId(@PathVariable("Id") long Id){
       logger.info("kamer. Id********* "+Id);
       return lightsService.getKamerbyId(Id);
   }



    @PostMapping(value="/light")
    public ResponseEntity<String> createNewLight(@RequestBody LightResource lightResource) throws LightsAppException {
        try{
            Light light = lightsService.lightToevoegen(lightResource);
            logger.info(" new light "+light);
            jmsTemplate.convertAndSend("springbootQueue", light);

            return ResponseEntity.status(HttpStatus.ACCEPTED).body(light.getNaam());
        }catch (LightsAppException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    @PutMapping(value="/updatelight/{id}")
    public ResponseEntity<String> updateLight(@PathVariable("id") long id, @RequestBody LightResource lightResource)throws LightsAppException {

        Light light = lightsService.lightUpdate(id, lightResource);

       logger.info(" updatelight "+light);
        jmsTemplate.convertAndSend("springbootQueue", light);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(light.getNaam());

    }

    @DeleteMapping(value="/deletelight/{id}")
    public Map<String, Boolean> deleteLight(@PathVariable("id")  long id) throws LightsAppException {
        Light light=lightsService.lightDelete(id);
        Map<String, Boolean> response = new HashMap<>();

        logger.info(" deleted light "+light);
        jmsTemplate.convertAndSend("springbootQueue", light);

        response.put(light.getNaam()+" is deleted", Boolean.TRUE);
               return response;
    }

}
