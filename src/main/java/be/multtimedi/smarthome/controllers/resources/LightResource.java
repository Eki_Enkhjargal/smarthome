package be.multtimedi.smarthome.controllers.resources;

public class LightResource {

    private String naam;
    private Long kamerId;
    private String isAan;

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Long getKamerId() {
        return kamerId;
    }

    public void setKamerId(Long kamerId) {
        this.kamerId = kamerId;
    }

    public String getIsAan() {
        return isAan;
    }

    public void setIsAan(String isAan) {
        this.isAan = isAan;
    }

    @Override
    public String toString() {
        return "LightResource{" +
                "naam='" + naam + '\'' +
                ", kamerId=" + kamerId +
                ", isAan='" + isAan + '\'' +
                '}';
    }
}
