INSERT INTO Lights (naam, isAan, kamerId) VALUES
	('Woonkamerlicht1', 'aan', 1),
	('Keukenlicht1', 'aan', 2),
	( 'Badkamerlicht1', 'aan', 3),
	('Slaapkamerlicht1', 'aan', 4);

INSERT INTO Kamers (Id, naam, kamerCode) VALUES
(1, 'Woonkamer', 'WNK'),
(2, 'Keuken', 'KKN'),
(3,  'Badkamer', 'BDK'),
(4, 'Slaapkamer', 'SLK');
