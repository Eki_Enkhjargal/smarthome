CREATE TABLE IF NOT EXISTS Lights (
    Id LONG IDENTITY NOT NULL,
    naam VARCHAR(100),
    isAan VARCHAR (100),
    kamerId LONG,
    primary key (Id),
    FOREIGN KEY (kamerId)
);

CREATE TABLE IF NOT EXISTS Kamers (
    Id LONG IDENTITY NOT NULL,
    naam VARCHAR(100),
    kamerCode VARCHAR (100)
    primary key (Id)
    );
