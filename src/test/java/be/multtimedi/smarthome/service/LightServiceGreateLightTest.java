package be.multtimedi.smarthome.service;

import be.multtimedi.smarthome.domain.Kamer;
import be.multtimedi.smarthome.domain.Light;
import be.multtimedi.smarthome.domain.LightBuilder;
import be.multtimedi.smarthome.exception.LightsAppException;
import be.multtimedi.smarthome.repository.KamersRepository;
import be.multtimedi.smarthome.repository.LightRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
@SpringJUnitConfig
public class LightServiceGreateLightTest {
    private static final Long ID = 1L;
    private static final String NAME = "BadkamerLight";
    private static final String ISAAN = "aan";
    private static final Kamer KAMER = new Kamer(3L, "Badkamer", "BDK");

    @MockBean
    private LightRepository lightRepository;

    @MockBean
    private KamersRepository kamersRepository;

    @InjectMocks
    private LightsService lightsService;

    private Light createdLight;
    private List<LightBuilder> lightList = new ArrayList<>();

    @BeforeEach
    public void init() {
        createdLight = new Light();
        createdLight.setNaam(NAME);
        createdLight.setIsAan(ISAAN);
        createdLight.setKamer(KAMER);
        createdLight.setId(ID);


    }

    @Test
    public void returnLightwhenSaveNewLigth() throws LightsAppException {
        when(lightRepository.findLightById(ID)).thenReturn(createdLight);
        assertEquals(createdLight, lightsService.lightById(ID));
    }

    @Test
    public void returnsNullWhenNoLightWhithKamerFound(){
       assertEquals(lightList, lightsService.getLightsvanKamer(KAMER.getId()));
    }
}
