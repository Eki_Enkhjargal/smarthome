package be.multtimedi.smarthome.service;

import be.multtimedi.smarthome.controllers.resources.LightResource;
import be.multtimedi.smarthome.domain.Kamer;
import be.multtimedi.smarthome.domain.Light;
import be.multtimedi.smarthome.exception.LightsAppException;
import be.multtimedi.smarthome.repository.KamersRepository;
import be.multtimedi.smarthome.repository.LightRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
//@ExtendWith(SpringExtension.class)
@SpringJUnitConfig
class LightsServiceTest {
    private static final Long ID = 1L;
    private static final String NAME = "BadkamerLight";
    private static final String ISAAN = "aan";
  //  private static final Kamer KAMER = new Kamer( 3L, "Badkamer");

    @MockBean
    private LightRepository lightRepository;

    @MockBean
    Logger logger;

    @MockBean
    private KamersRepository kamersRepository;

    @InjectMocks
    private LightsService lightsService;

    private Light light;
    private Kamer kamer;
    private LightResource lightResource;
    private List<Light> lightsList = new ArrayList<>();
    private List<Kamer> kamersList = new ArrayList<>();

    @BeforeEach
    public void init() {

        kamer = new Kamer();
        kamer.setId(ID);
        kamer.setCode("BDK");
        kamer.setNaam("Badkamer");

        light = new Light();
        light.setNaam(NAME);
        light.setIsAan(ISAAN);
        light.setKamer(kamer);
        light.setId(ID);

        lightResource = new LightResource();
        lightResource.setNaam(NAME);
        lightResource.setKamerId(kamer.getId());
        lightResource.setIsAan(ISAAN);


    }


    @Test
    void getAllLights() {
        assertEquals(lightsList, lightsService.getAllLights());

    }

    @Test
    void getAllKamers() {
        assertEquals(kamersList,lightsService.getAllKamers());
    }

    @Test
    void getKamerbyId() {
        when(kamersRepository.findKamerById(kamer.getId())).thenReturn(kamer);
        assertEquals(kamer, lightsService.getKamerbyId(ID));
    }

    @Test
    void getLightsVanKamer() {
        lightsList.add(light);
        when(kamersRepository.findKamerById(kamer.getId())).thenReturn(kamer);
        when(lightRepository.findLightByKamer(kamer)).thenReturn(lightsList) ;
        assertEquals(lightsList, lightsService.getLightsvanKamer(kamer.getId()));

    }
    @Test
    public void throwsLightsAppExceptionWhenNameNotGiven() {
        lightResource = new LightResource();
        lightResource.setNaam(" ");
        assertThrows(LightsAppException.class, ()->lightsService.lightToevoegen(lightResource));
    }
    @Test
    public void throwsLightsAppExceptionWhenIsAanNotGiven() {
        lightResource = new LightResource();
        lightResource.setIsAan(null);
        assertThrows(LightsAppException.class, ()->lightsService.lightToevoegen(lightResource));
    }
    @Test
    public void throwsLightsAppExceptionWhenKamerIdNotGiven() {
        lightResource = new LightResource();
        lightResource.setKamerId(null);
        assertThrows(LightsAppException.class, ()->lightsService.lightToevoegen(lightResource));
    }

    @Test
    void lightToevoegen() throws LightsAppException {


        Light newlight= new Light();
        newlight.setNaam(lightResource.getNaam());
        newlight.setAan(lightResource.getIsAan());
        newlight.setKamer(kamer);
        when(kamersRepository.findKamerById(lightResource.getKamerId())).thenReturn(kamer);
        when(lightRepository.save(newlight)).thenReturn(newlight);
        assertEquals(newlight.toString(),(lightsService.lightToevoegen(lightResource)).toString());

    }

    @Test
    void lightUpdate() throws LightsAppException {
        when(lightRepository.findLightById(ID)).thenReturn(light);
        when(kamersRepository.findKamerById(lightResource.getKamerId())).thenReturn(kamer);
        light.setKamer(kamer);
        light.setAan(lightResource.getIsAan());
        light.setNaam(lightResource.getNaam());
        when(lightRepository.save(light)).thenReturn(light);
        assertEquals(light, lightsService.lightUpdate(ID, lightResource));
    }

    @Test
    void lightDelete() throws LightsAppException {
        when(lightRepository.findLightById(ID)).thenReturn(light);
        assertEquals(light, lightsService.lightDelete(ID));
    }

    @Test
    void lightById() throws LightsAppException {

        when(lightRepository.findLightById(ID)).thenReturn(light);
        assertEquals(light, lightsService.lightById(ID));

    }
}