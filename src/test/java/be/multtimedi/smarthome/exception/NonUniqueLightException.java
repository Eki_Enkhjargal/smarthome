package be.multtimedi.smarthome.exception;

public class NonUniqueLightException extends Exception {

    public NonUniqueLightException(String message) {
        super(message);
    }
}
