package be.multtimedi.smarthome.domain;


import java.io.Serializable;


public class LightBuilder implements Serializable {

    private Long id;
    private String naam;
    private Kamer kamer;
    private String isAan;

    public LightBuilder(){

    }

    public LightBuilder(String naam, long id, String isAan) {
        this.naam = naam;
        this.id=id;
        this.isAan=isAan;
    }

    public static LightBuilder aLight() {
        return new LightBuilder();
    }

    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public String getIsAan() {
        return isAan;
    }

    public void setIsAan(String isAan) {
        this.isAan = isAan;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Kamer getKamer() {
        return kamer;
    }

    public void setKamer(Kamer kamer) {
        this.kamer = kamer;
    }

    public String isAan() {
        return isAan;
    }

    public void setAan(String aan) {
        isAan = aan;
    }



    public LightBuilder withId(Long id) {
        this.id=id;
        return this;
    }
    public LightBuilder withName(String naam) {
        this.naam = naam;
        return this;
    }
    public LightBuilder withKamer(Kamer kamer) {
        this.kamer=kamer;
        return this;
    }

    public LightBuilder withIsAan(String isAan) {
        this.isAan=isAan;
        return this;
    }

    public Light build(){
        Light light = new Light();
        light.setId(id);
        light.setNaam(naam);
        light.setIsAan(isAan);
        light.setKamer(kamer);
        return light;
    }
}
